import urllib2, os
from datetime import date
import subprocess
import json

def check_timestamp():
	'''
	Checks if the file containing the Top 40 songs has been modified in the last one day.
	Returns the number of days since the last change.
	'''
	#Check when the singles.json file was last modified
	modified = os.stat('singles.json').st_mtime
	sanitized_time = date.fromtimestamp(modified)

	#Check if more than one day has elapsed since the last file change
	days_elapsed = abs(date.today() - sanitized_time)
	return days_elapsed.days

def fetch_songs():
	'''
	Get the latest songs from the UK Top 40 singles API and stores them as a json file in the current directory
	'''
	url = 'http://ben-major.co.uk/labs/top40/api/singles/'
	data = urllib2.urlopen(url)
	#Check if the request was successful and write the request data to a file
	if data.getcode() == 200:
		data_string = str(data.read())
	else:
		print "Sorry, the URL could not be opened"

	try:
		f = open('singles.json', 'w')
		f.write(data_string)
	except Exception, e:
		raise e
	finally:
		f.close()

def top_five():
	with open('singles.json', 'r') as fd:
		text = fd.read()

	returndata = json.loads(text)
	singles = returndata['entries']

	top5 = [str(i['title']) + " "+ str(i['artist'])  for i in singles[:5]]
	ids = open('videos.txt', 'wb')

	for i in top5:
		args = "python search.py --q ".split()
		args.append(i)
		subprocess.Popen(args, stdout=ids)


def main():
	days = check_timestamp()
	if days >= 1:
		fetch_songs()
		top_five()

if __name__ == '__main__':
	main()